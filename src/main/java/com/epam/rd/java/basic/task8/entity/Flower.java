package com.epam.rd.java.basic.task8.entity;

import java.util.Objects;

public class Flower implements Comparable<Flower> {
    private String name;
    private String soil;
    private String origin;
    private VisualParameters visualParameters = new VisualParameters();
    private GrowingTips growingTips = new GrowingTips();
    private String multiplying;

    public Flower() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSoil() {
        return soil;
    }

    public void setSoil(String soil) {
        this.soil = soil;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public void setVisualParameters(VisualParameters visualParameters) {
        this.visualParameters = visualParameters;
    }

    public VisualParameters getVisualParameters() {
        return visualParameters;
    }

    public void setGrowingTips(GrowingTips growingTips) {
        this.growingTips = growingTips;
    }

    public GrowingTips getGrowingTips() {
        return growingTips;
    }

    public String getMultiplying() {
        return multiplying;
    }

    public void setMultiplying(String multiplying) {
        this.multiplying = multiplying;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Flower)) return false;

        Flower flower = (Flower) o;

        if (!Objects.equals(name, flower.name)) return false;
        if (!Objects.equals(soil, flower.soil)) return false;
        if (!Objects.equals(origin, flower.origin)) return false;
        if (!Objects.equals(visualParameters, flower.visualParameters))
            return false;
        if (!Objects.equals(growingTips, flower.growingTips)) return false;
        return Objects.equals(multiplying, flower.multiplying);
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (soil != null ? soil.hashCode() : 0);
        result = 31 * result + (origin != null ? origin.hashCode() : 0);
        result = 31 * result + (visualParameters != null ? visualParameters.hashCode() : 0);
        result = 31 * result + (growingTips != null ? growingTips.hashCode() : 0);
        result = 31 * result + (multiplying != null ? multiplying.hashCode() : 0);
        return result;
    }

    @Override
    public int compareTo(Flower o) {
        int cmp = this.name.compareTo(o.name);
        if (cmp != 0) {
            return cmp;
        }

        cmp = this.soil.compareTo(o.soil);
        if (cmp != 0) {
            return cmp;
        }

        cmp = this.origin.compareTo(o.origin);
        if (cmp != 0) {
            return cmp;
        }

        cmp = this.visualParameters.compareTo(o.visualParameters);
        if (cmp != 0) {
            return cmp;
        }

        cmp = this.growingTips.compareTo(o.growingTips);
        if (cmp != 0) {
            return cmp;
        }

        return this.multiplying.compareTo(o.multiplying);
    }
}