package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.Parammeter;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class ValueParser {
    private enum DefaultTypes {STRING, INTEGER, INT}

    public <E> boolean isDefaultTypes(E element, String fieldName) {
        final Field field = getFieldByName(element.getClass(), fieldName);
        if (field == null) {
            return false;
        }

        final String typeName = field.getType().getSimpleName().toLowerCase();
        for (DefaultTypes defaultType : DefaultTypes.values()) {
            if (typeName.equals(defaultType.name().toLowerCase())) {
                return true;
            }
        }

        return false;
    }

    public Map<String, String> getAttributes(Field field) {
        HashMap<String, String> attributes = new HashMap<>();
        Parammeter parameter = field.getAnnotation(Parammeter.class);

        if (parameter == null) {
            return attributes;
        }

        attributes.put(parameter.name(), parameter.value());

        return attributes;
    }

    public boolean isDefaultTypes(Field field) {
        final String typeName = field.getType().getSimpleName();

        for (DefaultTypes defaultType : DefaultTypes.values()) {
            if (typeName.equalsIgnoreCase(defaultType.name())) {
                return true;
            }
        }

        return false;
    }

    public <E> Object getNewInstance(E element, String fieldName) {
        final Field field = getFieldByName(element.getClass(), fieldName);
        if (field == null) {
            return null;
        }

        try {
            return field.getType().getConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
//            LOG.log(Level.WARNING, e.getMessage());
            System.err.println("Can't get new instance: " + e);
        }

        return null;
    }

    public <E> void setValue(E element, String fieldName, Object value) {
        final Field field = getFieldByName(element.getClass(), fieldName);
        if (field == null) {
            return;
        }

        field.setAccessible(true);
        setValueByType(field, element, value);
    }

      public <E> void modifyAttribute(E element, String fieldName, String name, String value) {
          final Field field = getFieldByName(element.getClass(), fieldName);
          if (field == null) {
              return;
          }

        Parammeter parameter = field.getAnnotation(Parammeter.class);
        if (parameter == null) {
            return;
        }

        InvocationHandler h = Proxy.getInvocationHandler(parameter);
        try {
            Field hField = h.getClass().getDeclaredField("memberValues");
            hField.setAccessible(true);
            Map<String, String> memberValues = (Map<String, String>) hField.get(h);
            memberValues.put("name", name);
            memberValues.put("value", value);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            System.out.println("Can't modify annotation: " + e);
        }
    }

    private Field getFieldByName(Class<?> clazz, String fieldName) {
        try {
            final Field field = clazz.getDeclaredField(fieldName);
            field.setAccessible(true);

            return field;
        } catch (NoSuchFieldException e) {
            return null;
        }
    }

    private <E> void setValueByType(Field field, E element, Object value) {
        try {
            if (value instanceof String) {
                field.set(element, convertValueToFieldType(field, value.toString()));
            } else {
                field.set(element, value);
            }
        } catch (IllegalAccessException e) {
//            LOG.log(Level.WARNING, e.getMessage());
            System.err.println("Can't set value to field: " + e);
        }
    }

    private Object convertValueToFieldType(Field field, String value) {
        switch (field.getType().getSimpleName()) {
            case "Integer":
            case "int":
                return Integer.parseInt(value);
            case "Double":
            case "double":
                return Double.parseDouble(value);
            default:
                return value;
        }
    }

    public <E> Object getFieldValue(Field field, E object) {
        try {
            return field.get(object);
        } catch (IllegalAccessException e) {
//            LOG.log(Level.WARNING, e.getMessage());
            System.err.println("Can't get value from field: " + e);
        }

        return null;
    }
}