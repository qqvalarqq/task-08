package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.Flower;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

public class WriterStAX {
    private final ValueParser valueParser = new ValueParser();


    public void write(List<Flower> flowers, String path) throws IOException, XMLStreamException {
        // creating factory
        final XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
        // getting StreamWriter
        try (FileWriter fileWriter = new FileWriter(path)) {
            final XMLStreamWriter xmlStreamWriter = outputFactory.createXMLStreamWriter(fileWriter);
            xmlStreamWriter.writeStartDocument();
            // adding xml file root element
            xmlStreamWriter.writeStartElement("flowers");
            xmlStreamWriter.writeAttribute("xmlns", "http://www.nure.ua");
            xmlStreamWriter.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            xmlStreamWriter.writeAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd ");

            // filling xml object
            for (Flower flower : flowers) {
                writerChildToXML(xmlStreamWriter, flower, "flower");
            }
            xmlStreamWriter.writeEndElement();
        }
    }

    private <E> void writerChildToXML(XMLStreamWriter writer, E object, String title) throws XMLStreamException {
        if (object == null) {
            return;
        }

        writer.writeStartElement(title);
        Class<?> clazz = object.getClass();
        for (Field field : clazz.getDeclaredFields()) {
            field.setAccessible(true);
            Object value = valueParser.getFieldValue(field, object);
            writeByTypeChild(writer, field, value);
        }
        writer.writeEndElement();
    }

    private <E> void writeByTypeChild(XMLStreamWriter writer, Field field, E object) throws XMLStreamException {
        if (valueParser.isDefaultTypes(field)) {
            writer.writeStartElement(field.getName());
            writeAttributes(writer, field);
            writeSimpleChild(writer, object);
            writer.writeEndElement();
        } else {
            writerChildToXML(writer, object, field.getName());
        }
    }

    private void writeAttributes(XMLStreamWriter writer, Field field) {
        final Map<String, String> parameters = valueParser.getAttributes(field);
        if (parameters.isEmpty()) {
            return;
        }

        for (Map.Entry<String, String> entry : parameters.entrySet()) {
            try {
                writer.writeAttribute(entry.getKey(), entry.getValue());
            } catch (XMLStreamException e) {
                System.out.println("Can't write attribute: " + e);
//            LOG.log(Level.WARNING, e.getMessage());
            }
        }
    }

    private void writeSimpleChild(XMLStreamWriter writer, Object object) throws XMLStreamException {
        if (object == null) {
            return;
        }
        writer.writeCharacters(object.toString());
    }

}
