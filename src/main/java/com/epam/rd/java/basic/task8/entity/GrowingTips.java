package com.epam.rd.java.basic.task8.entity;

import java.util.Objects;

public class GrowingTips implements Comparable<GrowingTips> {
    @Parammeter(name = "measure", value = "celcius")
    private int tempreture;
    @Parammeter(name = "lightRequiring", value = "yes")
    private String lighting;
    @Parammeter(name = "measure", value = "mlPerWeek")
    private int watering;

    public GrowingTips() {
    }

    public int getTempreture() {
        return tempreture;
    }

    public void setTempreture(int tempreture) {
        this.tempreture = tempreture;
    }

    public String getLighting() {
        return lighting;
    }

    public void setLighting(String lighting) {
        this.lighting = lighting;
    }

    public int getWatering() {
        return watering;
    }

    public void setWatering(int watering) {
        this.watering = watering;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GrowingTips)) return false;

        GrowingTips that = (GrowingTips) o;

        if (tempreture != that.tempreture) return false;
        if (watering != that.watering) return false;
        return Objects.equals(lighting, that.lighting);
    }

    @Override
    public int hashCode() {
        int result = tempreture;
        result = 31 * result + (lighting != null ? lighting.hashCode() : 0);
        result = 31 * result + watering;
        return result;
    }

    @Override
    public int compareTo(GrowingTips o) {
        int cmp = Integer.compare(this.tempreture, o.tempreture);
        if (cmp != 0) {
            return cmp;
        }

        return Integer.compare(this.watering, o.watering);
    }
}
