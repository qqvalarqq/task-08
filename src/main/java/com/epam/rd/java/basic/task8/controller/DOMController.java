package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.Flower;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController {
    private final String xmlFileName;
    private Document xmlDocument;

    // container for objects
    private final List<Flower> flowers = new ArrayList<>();
    private final ValueParser valueParser = new ValueParser();

    public DOMController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    // PLACE YOUR CODE HERE
    public void parse() {
        DocumentBuilderFactory dFactory = DocumentBuilderFactory.newInstance();
        dFactory.setNamespaceAware(true);
        dFactory.setValidating(true);

        try {
            dFactory.setFeature("http://xml.org/sax/features/validation", true);
            dFactory.setFeature("http://apache.org/xml/features/validation/schema", true);
        } catch (ParserConfigurationException e) {
            System.err.println(e.getLocalizedMessage());
        }

        try {
            DocumentBuilder builder = dFactory.newDocumentBuilder();
            builder.setErrorHandler(new DefaultHandler() {
                public void error(SAXParseException e) throws SAXException {
                    throw new SAXException("Some SAXParseException", e);
                }
            });

            xmlDocument = builder.parse(xmlFileName);
        } catch (SAXException | IOException | ParserConfigurationException e) {
            System.err.println(e.getLocalizedMessage());
        }
        NodeList nodes = xmlDocument.getElementsByTagName("flower");
        for (int i = 0; i < nodes.getLength(); i++) {
            NodeList childNodes = nodes.item(i).getChildNodes();
            Flower flower = new Flower();
            parse(childNodes, flower);

            flowers.add(flower);
        }
    }

    private <E> void parse(NodeList nodes, E element) {
        for (int i = 0; i < nodes.getLength(); i++) {
            final Node node = nodes.item(i);
//            parseAttributes(node, element);

            if (valueParser.isDefaultTypes(element, node.getNodeName())) {
                valueParser.setValue(element, node.getNodeName(), node.getTextContent());
            } else {
                final Object newElement = valueParser.getNewInstance(element, node.getNodeName());
                parse(node.getChildNodes(), newElement);
                valueParser.setValue(element, node.getNodeName(), newElement);
            }
        }
    }

    private <E> void parseAttributes(Node node, E element) {
        NamedNodeMap attributes = node.getAttributes();
        if (attributes == null || attributes.getLength() == 0) {
            return;
        }

        for (int i = 0; i < attributes.getLength(); i++) {
            valueParser.modifyAttribute(
                    element,
                    node.getNodeName(),
                    attributes.item(i).getNodeName(),
                    attributes.item(i).getNodeValue()
            );
        }
    }

    public void sort(Comparator<Flower> comparator) {
        flowers.sort(comparator);
    }

    /**
     * @throws TransformerException
     * @throws ParserConfigurationException
     */
    public void save(String outputXmlFile) throws TransformerException, ParserConfigurationException {
        StreamResult result = new StreamResult(new File(outputXmlFile));

        TransformerFactory tf = TransformerFactory.newInstance();
        javax.xml.transform.Transformer t = tf.newTransformer();
        t.setOutputProperty(OutputKeys.INDENT, "yes");

        t.transform(new DOMSource(new WriterDOM().getDocument(flowers)), result);
    }

}
