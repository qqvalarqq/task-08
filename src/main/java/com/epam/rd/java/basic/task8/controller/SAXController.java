package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.Flower;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {

    private String xmlFileName;
    private final ValueParser valueParser = new ValueParser();
    private final List<Flower> flowers = new ArrayList<>();
    private Flower flower;
    private String information;

    public SAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    // PLACE YOUR CODE HERE

    public void parse() {
        // obtaining factory
        final SAXParserFactory factory = SAXParserFactory.newInstance();
        factory.setNamespaceAware(true);
        factory.setValidating(true);

        try {
            factory.setFeature("http://xml.org/sax/features/validation", true);
            factory.setFeature("http://apache.org/xml/features/validation/schema", true);
        } catch (SAXNotRecognizedException | SAXNotSupportedException | ParserConfigurationException e) {
//            LOG.log(Level.WARNING, e.getMessage());
            System.err.println(e.getLocalizedMessage());
        }

        try {
            // obtaining parser
            final SAXParser parser = factory.newSAXParser();
            parser.parse(xmlFileName, this);
        } catch (SAXException | IOException | ParserConfigurationException e) {
//            LOG.log(Level.WARNING, e.getMessage());
            System.err.println(e.getLocalizedMessage());
        }
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
//		super.startElement(uri, localName, qName, attributes);
        if (qName.equals("flower")) {
            flower = new Flower();
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
//		super.endElement(uri, localName, qName);
        if (information == null) {
            return;
        }
        switch (qName) {
            case "name":
            case "soil":
            case "origin":
                valueParser.setValue(flower, qName, information);
                break;
            case "stemColour":
            case "leafColour":
            case "aveLenFlower":
                valueParser.setValue(flower.getVisualParameters(), qName, information);
                break;
            case "tempreture":
            case "watering":
                valueParser.setValue(flower.getGrowingTips(), qName, information);
                break;
            case "multiplying":
                valueParser.setValue(flower, qName, information);
                flowers.add(flower);
                break;
            default:
                information = null;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
//		super.characters(ch, start, length);
        information = new String(ch, start, length);
        information = information.replace("\n", "").trim();
    }


    public void sort(Comparator<Flower> comparator){
        flowers.sort(comparator);
    }

    public void save(String outputXmlFile) {
        try {
            new WriterStAX().write(flowers, outputXmlFile);
        } catch (IOException | XMLStreamException e) {
            System.err.println(e);
        }
    }
}