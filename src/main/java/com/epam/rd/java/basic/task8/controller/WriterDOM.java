package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.Flower;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

public class WriterDOM {
    private final ValueParser valueParser = new ValueParser();
    private Document document;

    public Document getDocument(List<Flower> flowers) throws ParserConfigurationException {
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        builderFactory.setNamespaceAware(true);
        DocumentBuilder documentBuilder = builderFactory.newDocumentBuilder();

        document = documentBuilder.newDocument();

        Element root = document.createElement("flowers");
        root.setAttribute("xmlns", "http://www.nure.ua");
        root.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        root.setAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd ");

        document.appendChild(root);
        for (Flower flower : flowers) {
            final Element currElement = document.createElement("flower");
            root.appendChild(getDocumentElement(currElement, flower));
        }

        return document;
    }

    private <E> Element getDocumentElement(Element currElement, E object) {
        if (object == null) {
            return null;
        }

        final Class<?> clazz = object.getClass();
        for (final Field field : clazz.getDeclaredFields()) {
            field.setAccessible(true);

            final Object value = getFieldValue(field, object);
            final Element childElement = getChildElement(field, value);

            final Map<String, String> parameters = valueParser.getAttributes(field);
            if (!parameters.isEmpty()) {
                parameters.forEach(childElement::setAttribute);
            }

            currElement.appendChild(childElement);
        }

        return currElement;
    }

    private <E> Element getChildElement(Field field, E object) {
        if (valueParser.isDefaultTypes(field)) {
            return get4SimpleChild(object, field);
        } else {
            return get4ComplexChild(object, field);
        }
    }

    private <E> Object getFieldValue(Field field, E object) {
        try {
            return field.get(object);
        } catch (IllegalAccessException e) {
//            LOG.log(Level.WARNING, e.getMessage());
        }
        return null;
    }

    private <E> Element get4SimpleChild(E object, Field field) {
        final Element childElement = document.createElement(field.getName());
        if (object != null) {
            childElement.setTextContent(object.toString());
        }
        return childElement;
    }

    private <E> Element get4ComplexChild(E object, Field field) {
        Element childElement = document.createElement(field.getName());
        getDocumentElement(childElement, object);

        return childElement;
    }
}
