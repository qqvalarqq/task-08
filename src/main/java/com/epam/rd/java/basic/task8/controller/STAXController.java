package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.Flower;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.stream.StreamSource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {
	private String xmlFileName;
	private final ValueParser valueParser = new ValueParser();
	private final List<Flower> flowers = new ArrayList<>();
	private Flower flower;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE
	public void parse() {
		XMLInputFactory factory = XMLInputFactory.newInstance();
		try {
			// creating event reader
			XMLEventReader eventReader = factory.createXMLEventReader(new StreamSource(xmlFileName));
			// checking value and filling Tariff object
			while (eventReader.hasNext()) {
				XMLEvent xmlEvent = eventReader.nextEvent();
				if (xmlEvent.isStartElement()) {
					StartElement startElement = xmlEvent.asStartElement();
					if (startElement.getName().getLocalPart().equals("flower")) {
						flower = new Flower();
						continue;
					}
					setValues(startElement, eventReader);
					// checking for XML object end
				} else if (xmlEvent.isEndElement()) {
					EndElement endElement = xmlEvent.asEndElement();
					if (endElement.getName().getLocalPart().equals("flower")) {
						flowers.add(flower);
					}
				}
			}
		} catch (XMLStreamException e) {
			System.err.println(e.getLocalizedMessage());
		}
	}


	private void setValues(StartElement startElement, XMLEventReader eventReader) throws XMLStreamException {
		String qName = startElement.getName().getLocalPart();
		switch (qName) {
			case "name":
			case "soil":
			case "origin":
			case "multiplying":
				valueParser.setValue(flower, qName, eventReader.nextEvent().asCharacters().getData());
				break;
			case "stemColour":
			case "leafColour":
			case "aveLenFlower":
				valueParser.setValue(flower.getVisualParameters(), qName, eventReader.nextEvent().asCharacters().getData());
				break;
			case "tempreture":
			case "watering":
				valueParser.setValue(flower.getGrowingTips(), qName, eventReader.nextEvent().asCharacters().getData());
				break;
			default:
		}
	}

	public void sort(Comparator<Flower> comparator){
		flowers.sort(comparator);
	}

	public void save(String outputXmlFile) {
		try {
			new WriterStAX().write(flowers, outputXmlFile);
		} catch (IOException | XMLStreamException e) {
			System.err.println(e);
		}
	}
}