package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.entity.Flower;

import java.util.Comparator;

public class Main {

    public static void main(String[] args) throws Exception {
        if (args.length != 1) {
            return;
        }

        String xmlFileName = args[0];
        System.out.println("Input ==> " + xmlFileName);

        ////////////////////////////////////////////////////////
        // DOM
        ////////////////////////////////////////////////////////

        // get container
        DOMController domController = new DOMController(xmlFileName);
        // PLACE YOUR CODE HERE
        domController.parse();

        // sort (case 1)
        // PLACE YOUR CODE HERE
        domController.sort(Flower::compareTo);

        // save
        String outputXmlFile = "output.dom.xml";
        // PLACE YOUR CODE HERE
        domController.save(outputXmlFile);

        ////////////////////////////////////////////////////////
        // SAX
        ////////////////////////////////////////////////////////

        // get
        SAXController saxController = new SAXController(xmlFileName);
        // PLACE YOUR CODE HERE
        saxController.parse();

        // sort  (case 2)
        // PLACE YOUR CODE HERE
        saxController.sort(Comparator.reverseOrder());

        // save
        outputXmlFile = "output.sax.xml";
        // PLACE YOUR CODE HERE

        saxController.save(outputXmlFile);

        ////////////////////////////////////////////////////////
        // StAX
        ////////////////////////////////////////////////////////

        // get
        STAXController staxController = new STAXController(xmlFileName);
        // PLACE YOUR CODE HERE
        staxController.parse();
        // sort  (case 3)
        // PLACE YOUR CODE HERE
		staxController.sort((o1, o2) -> o2.getName().compareTo(o1.getName()));

        // save
        outputXmlFile = "output.stax.xml";
        // PLACE YOUR CODE HERE
        staxController.save(outputXmlFile);
    }

}
