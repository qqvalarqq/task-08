package com.epam.rd.java.basic.task8.entity;

import java.util.Objects;

public class VisualParameters implements Comparable<VisualParameters> {
    private String stemColour;
    private String leafColour;
    @Parammeter(name = "measure", value = "cm")
    private int aveLenFlower;

    public VisualParameters() {
    }

    public String getStemColour() {
        return stemColour;
    }

    public void setStemColour(String stemColour) {
        this.stemColour = stemColour;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public void setLeafColour(String leafColour) {
        this.leafColour = leafColour;
    }

    public int getAveLenFlower() {
        return aveLenFlower;
    }

    public void setAveLenFlower(int aveLenFlower) {
        this.aveLenFlower = aveLenFlower;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof VisualParameters)) return false;

        VisualParameters that = (VisualParameters) o;

        if (aveLenFlower != that.aveLenFlower) return false;
        if (!Objects.equals(stemColour, that.stemColour)) return false;
        return Objects.equals(leafColour, that.leafColour);
    }

    @Override
    public int hashCode() {
        int result = stemColour != null ? stemColour.hashCode() : 0;
        result = 31 * result + (leafColour != null ? leafColour.hashCode() : 0);
        result = 31 * result + aveLenFlower;
        return result;
    }

    @Override
    public int compareTo(VisualParameters o) {
        int cmp = this.stemColour.compareTo(o.stemColour);
        if (cmp != 0) {
            return cmp;
        }

        cmp = this.leafColour.compareTo(o.leafColour);
        if (cmp != 0) {
            return cmp;
        }

        return Integer.compare(this.aveLenFlower, o.aveLenFlower);
    }
}
