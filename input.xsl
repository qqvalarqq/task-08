<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
    <html>
        <body>
            <h2>Flowers</h2>        
            <table border="1">
                <tr>
                    <td rowspan="2" align="center">Name</td>
                    <td rowspan="2" align="center">Soil</td>
                    <td rowspan="2" align="center">Origin</td>
                    <td colspan="3" align="center">Visual parameters</td>
                    <td colspan="3" align="center">Growing tips</td>
                    <td rowspan="2" align="center">Multiplying</td>
                </tr>
                <tr>
                    <td align="center">Stem colour</td>
                    <td align="center">Leaf colour</td>
                    <td align="center">Avelen flower<br/>(measure)</td>
                    <td align="center">Tempreture<br/>(measure)</td>
                    <td align="center">Lighting</td>
                    <td align="center">Watering<br/>(measure)</td>
                </tr>
                <xsl:for-each select="flowers/flower">
                    <tr>
                        <td><xsl:value-of select="name"/></td>
                        <td><xsl:value-of select="soil"/></td>
                        <td><xsl:value-of select="origin"/></td>
                        
                        <td><xsl:value-of select="visualParameters/stemColour"/></td>
                        <td><xsl:value-of select="visualParameters/leafColour"/></td>
                        <td><xsl:value-of select="visualParameters/aveLenFlower"/> (<xsl:value-of select="visualParameters/aveLenFlower/@measure"/>)</td>       
                        
                        <td><xsl:value-of select="growingTips/tempreture"/> (<xsl:value-of select="growingTips/tempreture/@measure"/>)</td>
                        <td><xsl:value-of select="growingTips/lighting/@lightRequiring"/></td>
                        <td><xsl:value-of select="growingTips/watering"/> (<xsl:value-of select="growingTips/watering/@measure"/>)</td>   
                        
                        <td><xsl:value-of select="multiplying"/></td>                         
                    </tr>
                </xsl:for-each>
            </table>
        </body>
    </html>
</xsl:template>
</xsl:stylesheet>    